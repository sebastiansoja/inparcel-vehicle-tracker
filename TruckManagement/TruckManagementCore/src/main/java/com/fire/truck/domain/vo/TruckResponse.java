package com.fire.truck.domain.vo;

import lombok.Value;

@Value
public class TruckResponse {

    String vehicleReg;

    boolean telematicsEnabled;
}
