package com.fire.truck.domain.vo;

import lombok.Value;

@Value
public class TruckRequest {

    String vehicleReg;

    boolean telematicsEnabled;
}
