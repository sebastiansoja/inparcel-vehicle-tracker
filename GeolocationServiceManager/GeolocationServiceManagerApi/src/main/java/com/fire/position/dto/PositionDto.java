package com.fire.position.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PositionDto {

    private String vehicleReg;
    private CoordinateDto coordinate;
    private String country;
    private String timestamp;


    public void updateCoordinates(CoordinateDto coordinate) {
        this.coordinate = coordinate;
    }

    public void updateCountry(String country) {
        this.country = country;
    }
}
