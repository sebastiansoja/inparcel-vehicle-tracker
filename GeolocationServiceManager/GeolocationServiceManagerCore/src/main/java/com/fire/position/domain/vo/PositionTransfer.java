package com.fire.position.domain.vo;

public record PositionTransfer(boolean telemetryEnabled, Double longitude, Double latitude) {

}
