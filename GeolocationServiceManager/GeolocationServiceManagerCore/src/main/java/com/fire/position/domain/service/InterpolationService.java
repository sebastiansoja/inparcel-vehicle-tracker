package com.fire.position.domain.service;

import com.fire.position.domain.model.Position;

public interface InterpolationService {
    Position interpolatePosition(Position currentPosition, Position previousPosition);
}
