package com.fire.geocoding.domain.vo;

import lombok.Value;

@Value
public class CountryResponse {
    String country;
}
